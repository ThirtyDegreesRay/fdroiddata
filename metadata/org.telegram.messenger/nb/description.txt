Flere proprietære deler ble fjernet fra den originale Telegram-klienten, inkludert Google Play-tjenester for plasseringstjenester, HockeySDK for selv-oppdateringer og push-merknader gjennom Google Cloud Messaging. Plasseringsdeling er gjenintrodusert ved bruk av OpenStreetMap.

Anti-funksjon: Ufritt nettverk, siden tjenerne kjører proprietær programvare.

