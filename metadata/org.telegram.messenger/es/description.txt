Mensajería pura e instantánea. Simple, rápida, segura y sincronizada a través de todos tus dispositivos. Más de 100 millones de usuarios activos en dos años y medio.

RÁPIDA: Telegram es la aplicación de mensajería más rápida del mercado. Conecta a las personas a través de una red única de centros de datos, distribuida a través de todo el planeta.

SINCRONIZADA: Accede a tus mensajes desde todos tus dispositivos al mismo tiempo. Puedes comenzar a escribir en tu teléfono y terminar el mensaje desde tu tablet o PC. Nunca pierdas tus datos de nuevo.

ILIMITADA: Puedes enviar archivos y multimedia sin límites de tamaño o tipo. El historial de tu chat no requerirá almacenamiento en tu dispositivo y estará guardado de forma segura en la nube de Telegram, durante el tiempo que lo necesites.

SEGURA: Nuestra misión es entregar la mejor seguridad sin perder la facilidad de uso. Todo en Telegram, incluyendo los chats, grupos, multimedia, etc., se cifra usando una combinación del cifrado 256-bit symmetric AES, el cifrado 2048-bit RSA, y el intercambio de claves seguras Diffie–Hellman.

PODEROSA: Puedes crear grupos de chat de hasta 5000 miembros, compartir vídeos largos, documentos de cualquier tipo (.DOC, .MP3, .ZIP, etc.), e incluso configurar bots para tareas específicas. Es la herramienta perfecta para organizar comunidades en línea y coordinar grupos de trabajo.

