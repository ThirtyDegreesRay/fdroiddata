Categories:Writing
License:MIT
Web Site:https://github.com/gsantner/markor/blob/HEAD/README.md
Source Code:https://github.com/gsantner/markor
Issue Tracker:https://github.com/gsantner/markor/issues
Changelog:https://github.com/gsantner/markor/blob/HEAD/CHANGELOG.md
Donate:https://gsantner.github.io/#donate
Bitcoin:1B9ZyYdQoY9BxMe9dRUEKaZbJWsbQqfXU5

Auto Name:Markor

Repo Type:git
Repo:https://github.com/gsantner/markor

Build:0.1.0,1
    commit=v0.1.0
    subdir=app
    submodules=yes
    gradle=FlavorDefault

Build:0.1.1,2
    commit=v0.1.1
    subdir=app
    submodules=yes
    gradle=FlavorDefault

Build:0.1.2,3
    commit=v0.1.2
    subdir=app
    submodules=yes
    gradle=FlavorDefault

Build:0.1.3,4
    commit=v0.1.3
    subdir=app
    submodules=yes
    gradle=FlavorDefault

Maintainer Notes:
Description and summary in git metadata submodule
.

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.3
Current Version Code:4
