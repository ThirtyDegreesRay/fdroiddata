Accéder à  l'interface de commande Linux (shell) de votre Android. Libérez le Geek qui est en vous!

Il s’agit d’une nouvelle version de l’application populaire « Android Terminal Emulator ». Le même programme, seulement renommé.

Fonctionnalités principales

+ Émulation complète du terminal Linux. + Multiples fenêtres. + Raccourcis du launcher. + Texte UTF-8 (arabe, mandarin, grec, hébreu, japonais, coréen, russe, thaï, etc.) + Totalement gratuit. Sans pubs, achats, fenêtres pop-up, rien.

FAQ rapide :

Envie d’en savoir plus sur l’émulateur de terminal pour Android ?

Rejoignez la communauté Google+ : #Android Terminal Emulator

https://plus.google.com/u/0/communities/106164413936367578283

Ou consultez le wiki de documentation :

http://github.com/jackpal/Android-Terminal-Emulator/wiki

Vous désirez ajouter ou améliorer une traduction de l'émulateur de terminal pour Android ? Allez sur https://github.com/jackpal/Android-Terminal-Emulator/wiki/Translating-to-Other-Languages et suivez les conseils.

